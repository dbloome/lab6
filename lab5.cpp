/*************************
Devin Bloomer
dbloome	
Lab 1
Lab Section: 1021-002
Hollis Liu & Emily Merritt
**************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
#include <ctime>
#include <cstdlib>

using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
	// IMPLEMENT as instructed below
	/*This is to seed the random generator */
	srand(unsigned (time(0)));
	//random_shuffle();

	Card cardarr[52];
	/*Create a deck of cards of size 52 (hint this should be an array) and
	*initialize the deck*/
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 13; j++)
		{
			cardarr[j+i*13].value = 2 + j;
			cardarr[j+i*13].suit = static_cast<Suit>(i);
		}
	}

	/*After the deck is created and initialzed we call random_shuffle() see the
	*notes to determine the parameters to pass in.*/
	random_shuffle(&cardarr[0], &cardarr[52], myrandom);

	/*Build a hand of 5 cards from the first five cards of the deck created
	*above*/
	Card temparr[5] = {cardarr[0], cardarr[1], cardarr[2], cardarr[3], cardarr[4]};
	
	/*Sort the cards.  Links to how to call this function is in the specs
	 *provided*/
	sort(temparr, temparr + 5, suit_order);

	/*Now print the hand below. You will use the functions get_card_name and
	 *get_suit_code */
	for(int i = 0; i < 5; i++)
	{
		cout << setw(10) << get_card_name(temparr[i]) << " of " << get_suit_code(temparr[i]) << endl;
	}

	
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
//determines who wins the draw
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  //determines the order of the values and suits
	if(lhs.suit < rhs.suit) { return true; }
	else if(lhs.suit > rhs.suit) { return false; }
	else
	{
		return (lhs.value < rhs.value);
	}

}
//function takes in a value and returns a unicode character
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return ""; 
  }
}

//labels each card 2-10, "jack" = 11, "queen" = 12, "king" = 13, "ace" = 14
string get_card_name(Card& c) {
  // IMPLEMENT
	//switching value with a string
	switch (c.value) {
	case 2: return "2";
	case 3: return "3";
	case 4: return "4";
	case 5: return "5";
	case 6: return "6";
	case 7: return "7";
	case 8: return "8";
	case 9: return "9";
	case 10: return "10";
	case 11: return "Jack";
	case 12: return "Queen";
	case 13: return "King";
	case 14: return "Ace";
	default: return "";	 }
}

